# Filter requires for junit mvn()-style requires as json_simple doesn't
# strictly need junit at runtime (it's a test-only dep)
%global junit junit:junit
%global mvn_requires_filter .*mvn\\(%{junit}\\)
%global __requires_exclude ^%{mvn_requires_filter}$

Name:           json_simple
Version:        1.1.1
Release:        23%{?dist}
Summary:        Simple Java toolkit for JSON
License:        ASL 2.0
URL:            http://code.google.com/p/json-simple/
BuildArch:      noarch

# svn export http://json-simple.googlecode.com/svn/tags/tag_release_1_1_1/ json-simple-1.1.1
# tar czf json-simple-1.1.1-src-svn.tar.gz json-simple-1.1.1
Source0:        json-simple-1.1.1-src-svn.tar.gz

#https://code.google.com/p/json-simple/issues/detail?id=97
Patch0:         json-simple-hash-java-1.8.patch

BuildRequires:  maven-local
BuildRequires:  mvn(junit:junit)
BuildRequires:  mvn(org.apache.felix:maven-bundle-plugin)

%description
JSON.simple is a simple Java toolkit for JSON. You can use JSON.simple 
to encode or decode JSON text. 
  * Full compliance with JSON specification (RFC4627) and reliable 
  * Provides multiple functionalities such as encode, decode/parse 
    and escape JSON text while keeping the library lightweight 
  * Flexible, simple and easy to use by reusing Map and List interfaces 
  * Supports streaming output of JSON text 
  * Stoppable SAX-like interface for streaming input of JSON text 
  * Heap based parser 
  * High performance (see performance testing) 
  * No dependency on external libraries 
  * Both of the source code and the binary are JDK1.2 compatible 

%package javadoc
Summary:       API documentation for %{name}

%description javadoc
This package contains %{summary}.

%package tests
Summary:       Tests jar for %{name}

%description tests
This package contains %{summary}.

%prep
%setup -q -n json-simple-%{version}
find . -name '*.jar' -exec rm -f '{}' \;
# All the files have dos line endings, remove them.
find . -type f -exec %{__sed} -i 's/\r//' {} \;

%patch0 -p1

%mvn_file : %{name}

%pom_xpath_inject pom:project/pom:build/pom:plugins '<plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-jar-plugin</artifactId>
        <version>3.1.2</version>
        <executions>
          <execution>
            <goals>
              <goal>test-jar</goal>
            </goals>
          </execution>
        </executions>
      </plugin>'

%mvn_package com.googlecode.json-simple:json-simple:jar:tests:%{version} tests

%build
%mvn_build

%install
%mvn_install

%files -f .mfiles
%doc AUTHORS.txt ChangeLog.txt LICENSE.txt README.txt

%files javadoc -f .mfiles-javadoc
%doc LICENSE.txt

%files tests -f .mfiles-tests

%changelog
* Mon Oct 07 2019 Severin Gehwolf <sgehwolf@redhat.com> - 1.1.1-23
- rebuilt

* Mon Oct 07 2019 Severin Gehwolf <sgehwolf@redhat.com> - 1.1.1-22
- Filter junit maven runtime requirement.

* Mon Oct 07 2019 Severin Gehwolf <sgehwolf@redhat.com> - 1.1.1-21
- Add tests sub-package.

* Mon Oct 07 2019 Severin Gehwolf <sgehwolf@redhat.com> - 1.1.1-20
- Initial import from Fedora.
